import React from 'react';

import { Navigator } from './Navigator/Navigator'


export const App = () => (
  <div>
    <Navigator />
  </div>
);
