import React from 'react'
import './style.css'
import { useTracker } from 'meteor/react-meteor-data'
import { useParams } from 'react-router-dom'


import { PaintBoardItem } from '../PaintBoardItem/PaintBoardItem'
import { roomsCollection } from '../../../api/rooms'


export const PaintBoard = () => {

    const { id } = useParams()

    // Находим текущую комнату по id из адресной строки.
    const rooms = useTracker(() => roomsCollection.find({}).fetch())
    const thisRoom = rooms.find((room) => room._id === id)

    return (
        <div className={'paint-board'}>
            <div className={'paint-board__body'}>
                {/* Проверка на undefind */}
                {thisRoom ?
                    thisRoom.blocks?.map((block) => {

                        return (
                            <PaintBoardItem
                                key={block.id}
                                id={block.id}
                                select={block.select}
                                room={thisRoom}
                            />
                        )
                    }
                    ) : ''}
            </div>

        </div>
    )
}