import React, { useState } from 'react';
import './style.css'
import { roomsCollection } from '../../../api/rooms';
import { useTracker } from 'meteor/react-meteor-data'

import { Link } from 'react-router-dom'

export const RoomItem = (props) => {

    // Находим активную комнату в коллекции
    const activeRoom = useTracker(() => roomsCollection.find({ isActive: true }).fetch())

    const removeRoom = () => {
        roomsCollection.remove(props.room._id)
    }
    const onClickButton = () => {
        removeRoom()
    }
    // Функция сброса активной комнаты 
    const resetActivityRoom = () => {
        activeRoom.forEach(room => {
            roomsCollection.update(room._id, {
                $set: {
                    isActive: false
                }
            })
        })
    }
    // Функция присваивает комнату статус активной
    const updateActivityRoom = () => {
        roomsCollection.update(props._id, {
            $set: {
                isActive: true
            }
        })
    }


    const onClickLink = () => {
        resetActivityRoom()
        updateActivityRoom()
    }

    return (

        <div className={props.isActive ? 'room-item active' : 'room-item'}>
            <Link onClick={onClickLink} type='radio' to={`${props.room._id}`} className={props.isActive ? 'roomitem-link link-active' : 'roomitem-link'}> {props.room.name}
            </Link>

            <button onClick={onClickButton} className={'room-item__delete-btn'}></button>
        </div >
    )
}