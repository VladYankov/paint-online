import React, { useState } from 'react';
import './style.css'
import { roomsCollection } from '../../../api/rooms'

import { useTracker } from 'meteor/react-meteor-data'

import { RoomItem } from "../RoomItem/RoomItem"


class Room {
    constructor(name, blocks) {
        this.name = name
        this.blocks = blocks
    }
}

export const RoomList = () => {
    const blocks = []
    const [roomName, setRoomName] = useState('')

    const rooms = useTracker(() => roomsCollection.find({}).fetch())

    const onChangeInput = (event) => {
        setRoomName(event.target.value)
    }

    // Функция создает 100 блоков для рисования и пушит их в пустой массив blocks
    const createBlocks = () => {
        for (let i = 1; i <= 100; i++) {
            blocks.push({ id: i, select: false })
        }
    }

    const onClickButton = () => {
        createBlocks()

        if (!roomName) {
            roomsCollection.insert(new Room('Безымянный рисунок', blocks))
        } else {
            roomsCollection.insert(new Room(roomName, blocks))
        }
        setRoomName('')
    }

    return (
        <div className={'roomlist__body'}>
            <div className={'roomlist__create-panel'}>
                <label className={'roomlist__text'}>
                    Название комнаты
                    <input
                        className={'roomlist__input'}
                        value={roomName}
                        type='text'
                        placeholder='Введите название'
                        onChange={onChangeInput} />
                </label>

                <button
                    type='submit'
                    className={'roomlist__btn-create'}
                    onClick={onClickButton}
                >Создать
                </button>
            </div>

            <div className={'roomlist__list'}>
                {rooms.map((room) => {
                    return (
                        <RoomItem
                            key={room.name}
                            id={room.id}
                            room={room}
                            _id={room._id}
                            isActive={room.isActive}
                        />
                    )
                })}
            </div>

        </div>
    )
}