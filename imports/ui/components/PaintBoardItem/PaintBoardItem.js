import './style.css'
import React, { useState } from 'react'

import { roomsCollection } from '../../../api/rooms'



export const PaintBoardItem = (props) => {

    const [select, setSelect] = useState(false)


    const thisBlocks = props.room.blocks

    const findIndexBlock = (event) => {
        return thisBlocks.findIndex((block) => block.id === +event.target.id)
    }
    // Функция находит изменяемый блок, изменяет его и заменяет в массиве блоков этой комнаты
    const changeBlock = (event) => {
        const indexBlock = findIndexBlock(event)
        const block = thisBlocks[indexBlock]

        const newBlock = { ...block, select: !select }
        thisBlocks.splice(indexBlock, 1, newBlock)
    }
    // Обновляет массив блоков в коллекции
    const updateRoomCollection = () => {
        roomsCollection.update(props.room._id, {
            $set: {
                blocks: thisBlocks
            }
        })
    }

    const onClickBlock = (event) => {
        changeBlock(event)
        updateRoomCollection()
        setSelect(!select)
    }

    return (
        <div
            className={props.select ? 'paint-board-item black' : 'paint-board-item white'}
            id={props.id}
            onClick={onClickBlock}
        >
        </div >
    )
}