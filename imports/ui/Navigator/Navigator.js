import React from 'react';
import './style.css'

import { PaintBoard } from '../components/PaintBoard/PaintBoard'
import { RoomList } from '../components/RoomList/RoomList'

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

export const Navigator = () => {
    return (
        <div className={'app'}>
            <Router>

                <RoomList />
                <div className={'paintboard-wrap'}>
                    {<Switch>
                        <Route path="/:id">
                            <PaintBoard />
                        </Route>
                    </Switch>}
                </div>


            </Router>
        </div>
    )
}